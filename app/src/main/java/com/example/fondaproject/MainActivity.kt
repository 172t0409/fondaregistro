package com.example.fondaproject

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.EditText
import androidx.appcompat.app.AlertDialog
import com.facebook.*
import com.facebook.login.LoginManager
import com.facebook.login.LoginResult
import com.google.android.gms.auth.api.signin.GoogleSignIn
import com.google.android.gms.auth.api.signin.GoogleSignInAccount
import com.google.android.gms.auth.api.signin.GoogleSignInClient
import com.google.android.gms.auth.api.signin.GoogleSignInOptions
import com.google.android.gms.common.api.ApiException
import com.google.android.gms.tasks.Task
import com.google.firebase.auth.*

class MainActivity : AppCompatActivity() {
    private val GOOGLE_SIGN_IN = 100
    private val callbackManager = CallbackManager.Factory.create()
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var btAc: Button
        var btRe: Button
        var btnG: Button
        var btnF: Button
        var txtEmail: EditText
        var txtPass: EditText

        btRe = findViewById(R.id.btnRegistrar)
        btAc = findViewById(R.id.btnAcceder)
        btnG = findViewById(R.id.btnGmail)
        btnF = findViewById(R.id.btnFacebook)
        txtEmail = findViewById(R.id.txtEmail)
        txtPass = findViewById(R.id.txtPass)


        btRe.setOnClickListener(View.OnClickListener {
            if (txtEmail.text.isNotEmpty() && txtPass.text.isNotEmpty()){
                FirebaseAuth.getInstance()
                        .createUserWithEmailAndPassword(txtEmail.text.toString(),
                            txtPass.text.toString()).addOnCompleteListener(this){
                                if (it.isSuccessful){
                                    val intent = Intent(applicationContext, Menu::class.java)
                                    startActivity(intent)
                                }else{
                                    print(txtEmail.text.toString() + " - " + txtPass.text.toString())
                                    val builder = AlertDialog.Builder(this)
                                    builder.setTitle("Error")
                                    builder.setMessage("Se ha producido un error")
                                    builder.setPositiveButton("Aceptar", null)
                                    val dialog: AlertDialog = builder.create()
                                    dialog.show()
                                }
                        }
            }
        })

        btAc.setOnClickListener(View.OnClickListener {
            if (txtEmail.text.isNotEmpty() && txtPass.text.isNotEmpty()){
                FirebaseAuth.getInstance()
                        .signInWithEmailAndPassword(txtEmail.text.toString(),
                                txtPass.text.toString()).addOnCompleteListener(this){
                            if (it.isSuccessful){
                                val intent = Intent(applicationContext, Menu::class.java)
                                startActivity(intent)
                            }else{
                                print(txtEmail.text.toString() + " - " + txtPass.text.toString())
                                val builder = AlertDialog.Builder(this)
                                builder.setTitle("Error")
                                builder.setMessage("Se ha producido un error")
                                builder.setPositiveButton("Aceptar", null)
                                val dialog: AlertDialog = builder.create()
                                dialog.show()
                            }
                        }
            }
        })

        btnG.setOnClickListener(View.OnClickListener {
            val googleConf: GoogleSignInOptions =
                GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                        .requestIdToken(getString(R.string.default_web_client_id))
                        .requestEmail()
                        .build()

            val googleClient : GoogleSignInClient = GoogleSignIn.getClient(this, googleConf)
            googleClient.signOut()

            startActivityForResult(googleClient.signInIntent, GOOGLE_SIGN_IN)

        })

        btnF.setOnClickListener {

            LoginManager.getInstance().logInWithReadPermissions(this, listOf("email"))

            LoginManager.getInstance().registerCallback(callbackManager,
                object: FacebookCallback<LoginResult>{

                    override fun onSuccess(result: LoginResult?) {

                        result?.let {

                            val token : AccessToken = it.accessToken

                            val credential : AuthCredential = FacebookAuthProvider.getCredential(token.token)

                            FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener{

                                if (it.isSuccessful){
                                    val intent = Intent(applicationContext, Menu::class.java)
                                    startActivity(intent)
                                }else{
                                    print("error")
                                }
                            }
                        }
                    }
                     override fun onCancel() {
                        print("error")
                    }

                    override fun onError(error: FacebookException?) {
                        print("error")
                }
            })
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {

        callbackManager.onActivityResult(requestCode, resultCode, data)
        super.onActivityResult(requestCode, resultCode, data)

        if(requestCode == GOOGLE_SIGN_IN){
            val task : Task<GoogleSignInAccount> = GoogleSignIn.getSignedInAccountFromIntent(data)
            try {
                val account : GoogleSignInAccount? = task.getResult(ApiException::class.java)

                if(account!=null){
                    val credential : AuthCredential = GoogleAuthProvider.getCredential(account.idToken, null)
                    FirebaseAuth.getInstance().signInWithCredential(credential).addOnCompleteListener{
                        if (it.isSuccessful){
                            val intent = Intent(applicationContext, Menu::class.java)
                            startActivity(intent)
                        }else{
                            val builder = AlertDialog.Builder(this)
                            builder.setTitle("Error")
                            builder.setMessage("Se ha producido un error")
                            builder.setPositiveButton("Aceptar", null)
                            val dialog: AlertDialog = builder.create()
                            dialog.show()
                        }
                    }
            }

            }catch (e: ApiException){
                val builder = AlertDialog.Builder(this)
                builder.setTitle("Error")
                builder.setMessage("Se ha producido un error")
                builder.setPositiveButton("Aceptar", null)
                val dialog: AlertDialog = builder.create()
                dialog.show()

            }

        }
    }


}