package com.example.fondaproject

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class bebidas : AppCompatActivity() {

    private var layoutManager: RecyclerView.LayoutManager? = null
    private var adapter: RecyclerView.Adapter<RecyclerViewAdapter3.ViewHolder>?=null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_bebidas)

        val lista: RecyclerView
        lista = findViewById(R.id.listBebidas)

        layoutManager = LinearLayoutManager( this)
        lista.layoutManager = layoutManager

        adapter = RecyclerViewAdapter3 ()
        lista.adapter = adapter
    }
}